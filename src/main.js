#!/usr/bin/env node

const src = __dirname

const WsMgr = require( `${src}/ws-manager` )
const log = console.log
const express = require( 'express' )

const HTTP_PORT = 8080

// Create the express app and tell it to
// set the headers for json
const app = express()

// Use the static directory for serving files
const staticDir = express.static( './static' )
app.use( staticDir)

// Set up the body-parser
const bodyParser = require( "body-parser" )
 
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json())

const wsMgrApi = WsMgr( app )

// Init. tools to get data from the stream
const stream = require( 'stream' );
const { promisify } = require( 'util' );
const got = require( 'got' )

function onRSVP( thing ) {
    log( thing )
}

async function collect() {
    const pl = promisify( stream.pipeline );

    const strm = got.stream( 'http://stream.meetup.com/2/rsvps' )
    pl( strm, onRSVP )
}

app.post( "/api/start", ( req, res ) => {
    console.log( req.body )
    const sec = ( req.body.seconds || 60 ) * 1000
    setTimeout( collect, sec )
})

app.listen( HTTP_PORT, () => {
    log( "Please Respond Running..." )
})

process.on('exit', function() {
    log( "Please Respond Shutting Down by Force..." )
});
 