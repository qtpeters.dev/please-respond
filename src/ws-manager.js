
const ws = require( 'ws' )
const WS_PORT = 3030

module.exports = ( app ) => {

    // Web Socket Server instance to work with Express
    const wsServer = new ws.Server({ 
      noServer: true 
    });

    // On connection, prepare to receive messages from a client
    wsServer.on( 'connection', ( sock, req ) => {
      console.log( "Connection Happened" )
      sock.on('message', message => console.log( message ) );
    });


    const server = app.listen( WS_PORT );
    server.on( 'upgrade', ( request, socket, head ) => {
      console.log( "Upgrade Happened" )
      wsServer.handleUpgrade( request, socket, head, socket => {
        wsServer.emit( 'connection', socket, request );
      });
    });


    return {


    }
}
